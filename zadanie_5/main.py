from operator import attrgetter
from process import Process
from random import randint

file = open('parameters.txt', 'r')
params = file.readline().split(' ')

numberOfTasks = int(params[0])
raiseEverySeconds = int(params[1])
minTime = int(params[2])
maxTime = int(params[3])
minArrivalTime = int(params[4])
maxArrivalTime = int(params[5])

print("Liczba zadań: {}".format(numberOfTasks))
print("Zmiana priorytetu co {} sekund".format(raiseEverySeconds))
print("Przedział czasu przetwarzania: [{}, {}]".format(minTime, maxTime))
print("Przedział czasu przybycia nowego zadania: [{}, {}]".format(minArrivalTime, maxArrivalTime))

newTasks = []
tasks = []

# Tworzenie pierwszego zadania, który zawsze będzie pierwszym przetwarzanym
firstProcess = Process(1, 0, randint(minTime, maxTime))
print("\nZadania do przetworzenia: ")
print(firstProcess)
tasks.append(firstProcess)

# Tworzenie nowych zadań
for j in range(1, numberOfTasks):
    process = Process(j + 1, randint(minArrivalTime, maxArrivalTime), randint(minTime, maxTime))
    newTasks.append(process)

# Wyświetlenie zadań
print(*newTasks, sep="\n")

# Algorytm egoistyczny
t = 0
lastIterTask = firstProcess
expropriations = 0
taskChanges = "1 "

print("\nProces przetwarzania: ")
while len(tasks) > 0:
    print("Czas = {}".format(t))
    # Wybór procesu z najwyższym priorytetem oraz przetworzenie go o sekundę
    maxPriorityTask = max(tasks, key=attrgetter('priority'))

    # Jeśli zadanie z najwyższym priorytetem nie ma większego priorytetu od tego z poprzedniej iteracji to przetwarzamy
    # zadanie z poprzedniej iteracji
    if maxPriorityTask.priority <= lastIterTask.priority and lastIterTask.time > 0:
        maxPriorityTask = lastIterTask

    tasks.remove(maxPriorityTask)
    maxPriorityTask.time -= 1
    maxPriorityTask.timeWaiting = 0

    print("Wybrane zadanie po przetworzeniu: ")
    print(maxPriorityTask)

    if maxPriorityTask.name != lastIterTask.name and lastIterTask.time > 0:
        expropriations += 1
        taskChanges += "! "

    if not maxPriorityTask.name == lastIterTask.name:
        taskChanges += str(maxPriorityTask.name) + " "

    lastIterTask = maxPriorityTask

    #Sprawdzenie, czy nie należy podniesc priorytetu oczekujących
    for task in tasks:
        if task.timeWaiting > 0 and task.timeWaiting % raiseEverySeconds == 0:
            task.priority += 1

        task.timeWaiting += 1

    # Jeśli przetwarzany w danym czasie proces nie został całkowicie przetworzony to powrót na listę
    if maxPriorityTask.time > 0:
        tasks.append(maxPriorityTask)

    print("Lista aktywnych zadań: ")
    print(*tasks, sep="\n")
    print("\n")

    # Zwiększenie czasu
    t += 1

    # Sprawdzenie, czy nie dochodzi nowy proces
    if len(newTasks) > 0:
        tasksToRemoveFromList = []
        for task in newTasks:
            if task.arrivalTime == t:
                tasks.append(task)
                tasksToRemoveFromList.append(task)

        if len(tasksToRemoveFromList) > 0:
            for taskToRemove in tasksToRemoveFromList:
                newTasks.remove(taskToRemove)

print(taskChanges)
print("Liczba wywłaszczeń: {}".format(expropriations))