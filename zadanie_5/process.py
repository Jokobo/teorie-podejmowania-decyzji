class Process:
    def __init__(self, name, arrivalTime, time):
        self.name = name
        self.arrivalTime = arrivalTime
        self.time = time
        self.timeWaiting = 0
        self.priority = 0

    def increasePriority(self, priorityInc):
        self.priority += priorityInc

    def __str__(self):
        return "Zadanie {}, długość realizacji {}, czas dotarcia {}, priorytet {}".format(self.name,
                                                                                          self.time,
                                                                                          self.arrivalTime,
                                                                                          self.priority)