# Teoria podejmowania decyzji
_Zadanie 2 (Wariant 4)_

**Autorzy:**
- Barbara Morawska 234096
- Andrzej Sasinowski 234118

Wydział Fizyki Technicznej, Informatyki i Matematyki Stosowanej

Politechnika Łódzka 

2019/2020 


###Wymagania 
Python > 3.6

###Uruchomienie programu (użytkownik)
Aby uruchomic program należy w wejść do katalogu, w którym znajdują
się pliki 

- ``main.py`` - poszukiwanie rozwiązania gry na podstawie macierzy wypłat
z pliku ``matrix.txt`` 

- ``lookingForA.py`` - znalezienie parametru A, dla którego można mówić o
strategii czystej i o strategii mieszanej, znalezienie parameru A, dla którego
przeciętna wygrana v = 4.
 
 a następnie wpisać komendę:

_Na systemie Windows_
```python
python [nazwa].py
```

_Na systemach Linux_
```python
python3 [nazwa].py
```

## Uruchomienie programu (developer)

#### Dodawanie wirtualnego środowiska w Pycharm

```
File ->
Settings->
Project (nazwa projektu) ->
Project interpreter ->
Koło zębate po prawej stronie obok pola "Project Interpreter" ->
Add ->
New environment ->
Ok
```

Następnie trzeba chwilkę poczekać i uruchomić terminal python na dole ekranu. W okno terminala należy wpisać:

```bash
pip install -r requirements.txt
```

co zainstaluje wymagane biblioteki.


#### Aktualizacja pliku requirements.txt

Po dodaniu biblioteki należy dodać ją też do pliku ``requirements.txt``, w taki sposób:

```bash
pip freeze > requirements.txt
```