from solutions import *
import numpy as np

# Wczytywanie danych z pliku
A = np.loadtxt("matrix.txt", dtype=np.float32)

minA = np.min(A)
maxA = np.max(A)
step = 0.1
values = np.arange(minA - step, maxA + step, step)

A[1, 0] = minA - 2 * step
prevPure, _, _ = solveInPureStrategy(A, logs=False)
if prevPure:
    print("Teraz jest strategia czysta:  (", minA - 2 * step, end='')
else:
    print("Teraz jest strategia mieszana:  (", minA - 2 * step, end='')

for i in values:
    A[1, 0] = i
    isPure, _, _ = solveInPureStrategy(A, logs=False)
    if prevPure == False and isPure == True:
        print(",", i - step, ")")
        print("Teraz jest strategia czysta: (", i, end='')
    elif prevPure == True and isPure == False:
        print(",", i - step, ")")
        print("Teraz jest strategia mieszana: (", i, end='')

    prevPure = isPure

print(",", maxA + step, ")")

print()

values = np.arange(-50, 50, 1)
epsilon = 0.000001

for i in values:
    A[1, 0] = i
    isPure, vA, vB = solveInPureStrategy(A, logs=False)
    if not isPure:
        A = removeDominatedParts(A, logs=False)
        vA, _ = planStrategyA(A, logs=False)
        vB, _ = planStrategyB(A, logs=False)
    if abs(4.0 - vA) < 0.00001:
            print("Wartość gry w A =", i, "wynosi", vA, "~~ 4.0")
