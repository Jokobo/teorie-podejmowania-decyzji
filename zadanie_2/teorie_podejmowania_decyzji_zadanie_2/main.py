from solutions import *

# Wczytywanie danych z pliku
A = np.loadtxt("matrix.txt", dtype=np.float32)

valuesHasBeenChanged = False
minA = np.min(A)

isPure, vA, vB = solveInPureStrategy(A)
if not isPure:
    if minA < 0:
        A += np.abs(minA)
        valuesHasBeenChanged = True

    A = removeDominatedParts(A)
    vA, XA = planStrategyA(A)
    vB, XB = planStrategyB(A)

    if valuesHasBeenChanged:
        vA -= np.abs(minA)
        vB -= np.abs(minA)

    print('Gracz A (wartość, częstotliwość):', vA, '\nX:', XA)
    print('Gracz B (wartość, częstotliwość):', vB, '\nX:', XB)



