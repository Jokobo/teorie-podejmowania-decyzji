import numpy as np
from scipy.optimize import linprog
import copy


def solveInPureStrategy(M: np.array, logs=True) -> [bool, float, float]:
    minA = float('inf')
    minAPos = ()
    for i in range(np.shape(M)[1]):
        maxA = np.max(M[:, i])
        maxAPos = (np.argmax(M[:, i]), i)
        if maxA < minA:
            minA = maxA
            minAPos = maxAPos

    if logs:
        print("Decyzja gracza A: ", minA, minAPos)

    maxB = -float('inf')
    maxBPos = ()
    for i in range(np.shape(M)[0]):
        minB = np.min(M[i, :])
        minBPos = (i, np.argmin(M[i, :]))
        if minB > maxB:
            maxB = minB
            maxBPos = minBPos

    if logs:
        print("Decyzja gracza B: ", maxB, maxBPos)

    if (minAPos == maxBPos) and (minA == maxB):
        if logs:
            print("Gra ma punkt siodłowy: ", minAPos, "v: ", minA)
            print("Decyzja ma rozwiązanie w zbiorze strategii czystych.")
        return True, minA, maxB

    if logs:
        print("Gra nie ma punktu siodłowego, więc nie ma rozwiązania w zbiorze strategii czystych.")
        print("Decyzja A:", minA, "decyzja B: ", maxB)
    return False, minA, maxB


def removeDominatedParts(M: np.array, logs=True) -> np.array:

    if logs:
        print()
    toRemove = []
    for i in range(np.shape(M)[1]):
        for j in range(i + 1, np.shape(M)[1]):
            graterOrEqual = M[:, i] <= M[:, j]
            oneGrater = M[:, i] < M[:, j]
            if not (False in graterOrEqual) and (True in oneGrater) and (j not in toRemove):
                toRemove.append(j)
                if logs:
                    print("Decyzja B" + str(j) + " była zdominowana przez B" + str(i))

    M = np.delete(M, toRemove, axis=1)

    toRemove = []

    for i in range(np.shape(M)[0]):
        for j in range(i + 1, np.shape(M)[0]):
            graterOrEqual = M[i, :] >= M[j, :]
            oneGrater = M[i, :] > M[j, :]
            if not (False in graterOrEqual) and (True in oneGrater) and (j not in toRemove):
                toRemove.append(j)
                if logs:
                    print("Decyzja A" + str(j) + " była zdominowana przez A" + str(i))\

    M = np.delete(M, toRemove, axis=0)

    if logs:
        print("Macierz po usunięciu części zdominowanych: ")
        print(M)
    return M


def planStrategyA(M: np.array, logs=True) -> [float, np.array]:
    if logs:
        print("\nAlgorytm sympleks")

    A = np.transpose(copy.deepcopy(M))
    b = np.repeat(1., np.shape(A)[0])
    b = -b
    A = -A
    c = np.repeat(1., np.shape(A)[1])

    bounds = []
    for i in range(len(c)):
        bounds.append((0., None))

    res = linprog(c, A_ub=A, b_ub=b,
                  bounds=bounds)

    v = 1 / sum(res.x)
    X = []
    for x in res.x:
        X.append(x * v)

    return v, X


def planStrategyB(M: np.array, logs=True) -> [float, np.array]:
    A = copy.deepcopy(M)
    b = np.repeat(1., np.shape(A)[0])
    c = np.repeat(1., np.shape(A)[1])
    c = -c

    bounds = []
    for i in range(len(c)):
        bounds.append((0., None))

    res = linprog(c, A_ub=A, b_ub=b,
                  bounds=bounds)

    v = 1 / sum(res.x)
    X = []
    for x in res.x:
        X.append(x * v)

    return v, X
