from graph import Graph
from node import Node
from edge import Edge

file = open("data.txt", "r")

# Plik
graph = Graph("Graf")

for line in file.read().splitlines():
    tempArray = line.split(' ')
    node = Node(tempArray[0])
    graph.addNode(node)
    node = Node(tempArray[2])
    graph.addNode(node)
    edge = Edge(tempArray[0], tempArray[2], float(tempArray[3]), tempArray[1])
    graph.addEdge(edge)

# Wypisz graf
graph.printAllNodes()
graph.printAllEdges()

# Po znalezieniu ścieżki optymalnej
graph.getBestPath()
graph.printBestPath()