import math


class Graph:
    def __init__(self, name):
        self.name = name
        self.nodes = []
        self.edges = []
        self.criticalPathEdges = []
        self.bestPathEdges = []
        self.bestValue = 0

    def addNode(self, node):
        exist = False
        for e_node in self.nodes:
            if e_node.name == node.name:
                exist = True
                break
        if not exist:
            self.nodes.append(node)

    def addEdge(self, edge):
        self.edges.append(edge)

    def getPreviousEdgesForGivenNode(self, toNode):
        previousEdges = []
        for edge in self.edges:
            if edge.toNode == toNode:
                previousEdges.append(edge)
        return previousEdges

    def getNextEdgesForGivenNode(self, fromNode):
        nextEdges = []
        for edge in self.edges:
            if edge.fromNode == fromNode:
                nextEdges.append(edge)
        return nextEdges

    def getPreviousNodeIndex(self, edge):
        for i in range(0, len(self.nodes)):
            if self.nodes[i].name == edge.fromNode:
                return i

    def getNextNodeIndex(self, edge):
        for i in range(0, len(self.nodes)):
            if self.nodes[i].name == edge.toNode:
                return i

    def getEdgeValue(self, fromNode, toNode):
        for edge in self.edges:
            if edge.fromNode == fromNode and edge.toNode == toNode:
                return edge.value

    def getCriticalPath(self):
        criticalPathEdges = []

        done = False
        i = 0
        while not done:
            nextEdges = self.getNextEdgesForGivenNode(self.nodes[i].name)
            for edge in nextEdges:
                testedNodeIndex = self.getNextNodeIndex(edge)
                condA = (self.nodes[i].earlyTime == self.nodes[i].lateTime)
                condB = (self.nodes[testedNodeIndex].earlyTime == self.nodes[testedNodeIndex].lateTime)
                condC = ((self.nodes[testedNodeIndex].earlyTime - self.nodes[i].earlyTime) == edge.time)
                if condA and condB and condC:
                    criticalPathEdges.append(edge)
                    i = testedNodeIndex
                    break
            if i == (len(self.nodes) - 1):
                done = True

        self.criticalPathEdges = criticalPathEdges


    def getBestPath(self):

        done = False
        i = len(self.nodes) - 1
        self.nodes[i].bestPath.append(self.nodes[i].name)
        i -= 1
        while not done:
            currBestPath = []
            currBestValue = 1000
            nextEdges = self.getNextEdgesForGivenNode(self.nodes[i].name)
            for edge in nextEdges:
                currPath = []
                currPath.append(self.nodes[i].name)
                nextNodeIndex = self.getNextNodeIndex(edge)
                val = edge.value + self.nodes[nextNodeIndex].bestPathValue
                currPath.extend(self.nodes[nextNodeIndex].bestPath)
                print("Węzeł {}: ścieżka: {}, wartość: {}".format(self.nodes[i].name, currPath, val))
                if val < currBestValue:
                    currBestValue = val
                    currBestPath = currPath
            self.nodes[i].bestPath = currBestPath
            self.nodes[i].bestPathValue = currBestValue
            #print(currBestPath)
            i -= 1
            if i == -1:
                done = True

        self.bestPathEdges = self.nodes[0].bestPath
        self.bestValue = self.nodes[0].bestPathValue

    def printBestPath(self):
        print(self.name + ":")
        pathString = self.bestPathEdges[0]
        for i in range(1, len(self.bestPathEdges)):
            pathString += " -> "
            pathString += self.bestPathEdges[i]

        print(pathString)
        print("Wartość dla najbardziej optymalnej ścieżki ścieżki: {}".format(self.bestValue))

    def printAllNodes(self):
        print(self.name + ":")
        for node in self.nodes:
            print("Node {}: {} o wartości {}".format(node.name, node.bestPath, node.bestPathValue))

    def printAllEdges(self):
        print(self.name + ":")
        for edge in self.edges:
            print("Edge {} ({}->{}): {}".format(edge.name, edge.fromNode, edge.toNode, edge.value))
