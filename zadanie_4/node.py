class Node:
    def __init__(self, name):
        self.name = name
        self.bestPath = []
        self.bestPathValue = 0

    def setBestPath(self, arr):
        self.bestPath = arr

    def setBestPathValue(self, val):
        self.bestPathValue = val

