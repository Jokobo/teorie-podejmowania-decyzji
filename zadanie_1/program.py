import criterias

file = open("matrix.txt", "r")
laplaceFile = open("laplace_factors.txt", "r")
alphaFile = open("hurwicz_alpha.txt", "r")
payoffTable = []
alpha = 1.0
laplaceWeights = []

alpha = float(alphaFile.readline())

for line in laplaceFile.read().splitlines():
    for element in line.split(' '):
        laplaceWeights.append(float(element))

for line in file.read().splitlines():
    tempArr = []
    for element in line.split(' '):
        tempArr.append(float(element))
    payoffTable.append(tempArr)
    ##payoffTable.append(line.split(' '))

print('\n'.join([''.join(['{:5}'.format(element) for element in row]) for row in payoffTable]))

decisionIndexWald, decisionValueWald = criterias.WaldMaximinModel(payoffTable)
decisionIndexMaximax, decisionValueMaximax = criterias.MaximaxModel(payoffTable)
decisionIndexMinimaxRegret, decisionValueMinimaxRegret = criterias.SavageMinimaxRegretModel(payoffTable)
decisionIndexLaplaceAvg, decisionValueLaplaceAvg = criterias.BayesLaplaceAverageModel(payoffTable, laplaceWeights)
decisionIndexHurwicz, decisionValueHurwicz = criterias.HurwiczWeightedAverageModel(payoffTable, alpha)

print("Najlepsza decyzja według kryterium Walda: {} z wartością równą {}".format(decisionIndexWald,
                                                                                 decisionValueWald))
print("Najlepsza decyzja według kryterium Maximax: {} z wartością równą {}".format(decisionIndexMaximax,
                                                                                   decisionValueMaximax))
print("Najlepsza decyzja według kryterium Savage'a: {} z wartością równą {}".format(decisionIndexMinimaxRegret,
                                                                                    decisionValueMinimaxRegret))
print("Najlepsza decyzja według kryterium Bayes-Laplace'a przy użyciu wag prawdopodobieństwa równych {}: "
      "{} z wartością równą {}".format(laplaceWeights, decisionIndexLaplaceAvg, decisionValueLaplaceAvg))

print("Najlepsza decyzja według kryterium Hurwicza przy użyciu współczynnika równego {}: {} z wartością równą {}"
      .format(alpha, decisionIndexHurwicz, decisionValueHurwicz))

file.close()
laplaceFile.close()