# Teoria podejmowania decyzji
_Zadanie 1 (Wariant 4)_

**Autorzy:**
- Barbara Morawska 234096
- Andrzej Sasinowski 234118

Wydział Fizyki Technicznej, Informatyki i Matematyki Stosowanej

Politechnika Łódzka 

2019/2020 


###Wymagania 
Python > 3.6

###Uruchomienie programu
Aby uruchomic program należy w wejść do katalogu, w którym znajduje
się plik ``program.py``, a następnie wpisać komendę:

_Na systemie Windows_
```python
python program.py
```

_Na systemach Linux_
```python
python3 program.py
```

Dodatkowo ustawienia parametrów działania algorytmów odbywa się za pomocą 
plików tekstowych:

| Nazwa pliku         | Opis zawartości pliku                                                                                                                                                                                                         |   |   |   |
|---------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|---|---|
| matrix.txt          | plik, w którym należy wpisać rozważaną macierz. Wartości w kolejnych  kolumnach powinny być oddzielone spacjami, natomiast w kolejnych rzędach  - enterami.                                                                   |   |   |   |
| laplace_factors.txt | plik, w którym należy wpisać współczynniki dla kryterium  Bayesa-Laplace'a, oznaczające prawdopodobieństwo wystąpienia danego  stanu natury. Współczynniki powinny być oddzielone spacjami oraz sumować  się do wartości 1.0. |   |   |   |
| hurwicz_alpha.txt   | plik, w którym należy wpisać współczynnik alfa dla kryterium Hurwicza (z przedziału [0.0, 1.0])                                                                                                                               |   |   |   |