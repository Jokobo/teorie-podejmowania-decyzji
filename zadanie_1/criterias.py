import copy


def WaldMaximinModel(payoffArray):
    rows = len(payoffArray)
    columns = len(payoffArray[0])

    minInRows = []
    for row in range(rows):
        minInRows.append(payoffArray[row][0])

    for row in range(rows):
        for column in range(columns):
            if payoffArray[row][column] < minInRows[row]:
                minInRows[row] = payoffArray[row][column]

    decisionIndex = [0]
    maxFromMins = minInRows[0]

    for row in range(1, rows):
        if minInRows[row] > maxFromMins:
            maxFromMins = minInRows[row]
            decisionIndex[0] = row
            if len(decisionIndex) > 1:
                decisionIndex = decisionIndex[0:1]
        elif minInRows[row] == maxFromMins:
            decisionIndex.append(row)

    decisionIndex = [i + 1 for i in decisionIndex]

    return decisionIndex, maxFromMins


def MaximaxModel(payoffArray):
    rows = len(payoffArray)
    columns = len(payoffArray[0])

    maxInRows = []
    for row in range(rows):
        maxInRows.append(payoffArray[row][0])

    for row in range(rows):
        for column in range(columns):
            if payoffArray[row][column] > maxInRows[row]:
                maxInRows[row] = payoffArray[row][column]

    decisionIndex = [0]
    maxFromMaxs = maxInRows[0]

    for row in range(1, rows):
        if maxInRows[row] > maxFromMaxs:
            maxFromMaxs = maxInRows[row]
            decisionIndex[0] = row
            if len(decisionIndex) > 1:
                decisionIndex = decisionIndex[0:1]
        elif maxInRows[row] == maxFromMaxs:
            decisionIndex.append(row)

    decisionIndex = [i + 1 for i in decisionIndex]

    return decisionIndex, maxFromMaxs


def SavageMinimaxRegretModel(payoffArray):
    rows = len(payoffArray)
    columns = len(payoffArray[0])

    maxInColumns = []
    for column in range(columns):
        maxValue = payoffArray[0][column];
        for row in range(rows):
            if payoffArray[row][column] > maxValue:
                maxValue = payoffArray[row][column]
        maxInColumns.append(maxValue)

    regretArray = copy.deepcopy(payoffArray)

    for column in range(columns):
        for row in range(rows):
            regretArray[row][column] = round(maxInColumns[column] - payoffArray[row][column], 2)

    maxInRegretRows = []
    for row in range(rows):
        maxInRegretRows.append(regretArray[row][0])

    for row in range(rows):
        for column in range(columns):
            if regretArray[row][column] > maxInRegretRows[row]:
                maxInRegretRows[row] = regretArray[row][column]

    decisionIndex = [0]
    minFromMaxsRegret = maxInRegretRows[0]

    for row in range(1, rows):
        if maxInRegretRows[row] < minFromMaxsRegret:
            minFromMaxsRegret = maxInRegretRows[row]
            decisionIndex[0] = row
            if len(decisionIndex) > 1:
                decisionIndex = decisionIndex[0:1]
            elif maxInRegretRows[row] == minFromMaxsRegret:
                decisionIndex.append(row)

    decisionIndex = [i + 1 for i in decisionIndex]

    return decisionIndex, minFromMaxsRegret


def BayesLaplaceAverageModel(payoffArray, laplaceWeights):
    rows = len(payoffArray)
    columns = len(payoffArray[0])

    averageInRows = []
    for row in range(rows):
        averageInRows.append(0.0)

    for row in range(rows):
        for column in range(columns):
            averageInRows[row] = averageInRows[row] + laplaceWeights[column] * payoffArray[row][column]
        averageInRows[row] = round(averageInRows[row], 3)

    decisionIndex = [0]
    maxInAverages = averageInRows[0]

    for row in range(1, rows):
        if averageInRows[row] > maxInAverages:
            maxInAverages = averageInRows[row]
            decisionIndex[0] = row
            if len(decisionIndex) > 1:
                decisionIndex = decisionIndex[0:1]
        elif averageInRows[row] == maxInAverages:
            decisionIndex.append(row)

    decisionIndex = [i + 1 for i in decisionIndex]

    return decisionIndex, maxInAverages


def HurwiczWeightedAverageModel(payoffArray, alpha):
    rows = len(payoffArray)
    columns = len(payoffArray[0])

    weightedAverageInRows = []
    for row in range(rows):
        weightedAverageInRows.append(0.0)

    for row in range(rows):
        minValue = payoffArray[row][0]
        maxValue = payoffArray[row][0]
        for column in range(columns):
            if payoffArray[row][column] > maxValue:
                maxValue = payoffArray[row][column]
            elif payoffArray[row][column] < minValue:
                minValue = payoffArray[row][column]
        weightedAverageInRows[row] = round(alpha * maxValue + (1.0 - alpha) * minValue, 3)

    decisionIndex = [0]
    maxInWeightedAverages = weightedAverageInRows[0]
    for row in range(1, rows):
        if weightedAverageInRows[row] > maxInWeightedAverages:
            maxInWeightedAverages = weightedAverageInRows[row]
            decisionIndex[0] = row
            if len(decisionIndex) > 1:
                decisionIndex = decisionIndex[0:1]
        elif weightedAverageInRows[row] == maxInWeightedAverages:
            decisionIndex.append(row)

    decisionIndex = [i + 1 for i in decisionIndex]

    return decisionIndex, maxInWeightedAverages
