# Teoria podejmowania decyzji
_Zadanie 3 (Wariant 4)_

**Autorzy:**
- Barbara Morawska 234096
- Andrzej Sasinowski 234118

Wydział Fizyki Technicznej, Informatyki i Matematyki Stosowanej

Politechnika Łódzka 

2019/2020 


###Wymagania 
Python > 3.6

###Pliki z danymi
Dane wejściowe znajdują się w plikach ``data_A.txt`` (dla wariantu A) oraz ``data_B.txt`` (dla wariantu B).
Dane zapisane są w zbioru zestawów liczb oddzielonych spacjami, gdzie każdy zestaw liczb reprezentuje daną czynność.


###Uruchomienie programu (użytkownik)
Aby uruchomic program należy w wejść do katalogu, w którym znajdują
się pliki 

- ``main.py`` - wczytanie danych z pliku ``data_A.txt`` oraz ``data_B.txt``, a następnie rozwiązanie zadania. 

- ``graph.py`` - klasa reprezentująca dane przedsięwzięcie.

- ``edge.py`` - klasa reprezentująca czynność (krawędź grafu).

- ``node.py`` - klasa reprezentująca proces (węzeł grafu).
 
 a następnie wpisać komendę:

_Na systemie Windows_
```python
python main.py
```

_Na systemach Linux_
```python
python3 main.py
```

## Uruchomienie programu (developer)

#### Dodawanie wirtualnego środowiska w Pycharm

```
File ->
Settings->
Project (nazwa projektu) ->
Project interpreter ->
Koło zębate po prawej stronie obok pola "Project Interpreter" ->
Add ->
New environment ->
Ok
```

Następnie trzeba chwilkę poczekać i uruchomić terminal python na dole ekranu. W okno terminala należy wpisać:

```bash
pip install -r requirements.txt
```

co zainstaluje wymagane biblioteki.


#### Aktualizacja pliku requirements.txt

Po dodaniu biblioteki należy dodać ją też do pliku ``requirements.txt``, w taki sposób:

```bash
pip freeze > requirements.txt
```