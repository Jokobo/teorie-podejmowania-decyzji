class Node:
    def __init__(self, name):
        self.name = name
        self.earlyTime = 0
        self.lateTime = 0

    def setEarlyTime(self, et):
        self.earlyTime = et

    def setLateTime(self, lt):
        self.lateTime = lt

