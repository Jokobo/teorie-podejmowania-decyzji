def calculateMeanTime(a, m, b):
    return (a + 4 * m + b) / 6.0


def calculateVariance(a, b):
    return ((b - a) / 6.0) * ((b - a) / 6.0)


class Edge:
    def __init__(self, fromNode, toNode, a, m, b):
        self.fromNode = fromNode
        self.toNode = toNode
        self.time = calculateMeanTime(a, m, b)
        self.variance = calculateVariance(a, b)
