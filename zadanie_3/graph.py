import math


class Graph:
    def __init__(self, name):
        self.name = name
        self.nodes = []
        self.edges = []
        self.criticalPathEdges = []
        self.time = 0
        self.stddev = 0

    def addNode(self, node):
        exist = False
        for e_node in self.nodes:
            if e_node.name == node.name:
                exist = True
                break
        if not exist:
            self.nodes.append(node)

    def addEdge(self, edge):
        self.edges.append(edge)

    def getPreviousEdgesForGivenNode(self, toNode):
        previousEdges = []
        for edge in self.edges:
            if edge.toNode == toNode:
                previousEdges.append(edge)
        return previousEdges

    def getNextEdgesForGivenNode(self, fromNode):
        nextEdges = []
        for edge in self.edges:
            if edge.fromNode == fromNode:
                nextEdges.append(edge)
        return nextEdges

    def getPreviousNodeIndex(self, edge):
        for i in range(0, len(self.nodes)):
            if self.nodes[i].name == edge.fromNode:
                return i

    def getNextNodeIndex(self, edge):
        for i in range(0, len(self.nodes)):
            if self.nodes[i].name == edge.toNode:
                return i

    def calculateEarlyTimes(self):
        for i in range(1, len(self.nodes)):
            previousEdges = self.getPreviousEdgesForGivenNode(self.nodes[i].name)
            if len(previousEdges) == 1:
                index = self.getPreviousNodeIndex(previousEdges[0])
                self.nodes[i].earlyTime = self.nodes[index].earlyTime + previousEdges[0].time
            elif len(previousEdges) > 1:
                times = []
                for edge in previousEdges:
                    index = self.getPreviousNodeIndex(edge)
                    time = self.nodes[index].earlyTime + edge.time
                    times.append(time)
                self.nodes[i].earlyTime = max(times)

    def calculateLateTimes(self):
        self.nodes[-1].lateTime = self.nodes[-1].earlyTime
        for i in reversed(range(1, len(self.nodes) - 1)):
            nextEdges = self.getNextEdgesForGivenNode(self.nodes[i].name)
            if len(nextEdges) == 1:
                index = self.getNextNodeIndex(nextEdges[0])
                self.nodes[i].lateTime = self.nodes[index].lateTime - nextEdges[0].time
            elif len(nextEdges) > 1:
                times = []
                for edge in nextEdges:
                    index = self.getNextNodeIndex(edge)
                    time = self.nodes[index].lateTime - edge.time
                    times.append(time)
                self.nodes[i].lateTime = min(times)

    def getCriticalPath(self):
        criticalPathEdges = []

        done = False
        i = 0
        while not done:
            nextEdges = self.getNextEdgesForGivenNode(self.nodes[i].name)
            for edge in nextEdges:
                testedNodeIndex = self.getNextNodeIndex(edge)
                condA = (self.nodes[i].earlyTime == self.nodes[i].lateTime)
                condB = (self.nodes[testedNodeIndex].earlyTime == self.nodes[testedNodeIndex].lateTime)
                condC = ((self.nodes[testedNodeIndex].earlyTime - self.nodes[i].earlyTime) == edge.time)
                if condA and condB and condC:
                    criticalPathEdges.append(edge)
                    i = testedNodeIndex
                    break
            if i == (len(self.nodes) - 1):
                done = True

        self.criticalPathEdges = criticalPathEdges

    def calculateStdDevFromCriticalPath(self):
        totalVariance = 0
        for edge in self.criticalPathEdges:
            totalVariance += edge.variance
            self.time += edge.time
        self.stddev = math.sqrt(totalVariance)

    def calculateZScoreForGivenValue(self, deadline):
        zScore = (deadline - self.time) / self.stddev
        return zScore

    def printCriticalPath(self):
        print(self.name + ":")
        pathString = self.nodes[0].name
        for edge in self.criticalPathEdges:
            pathString += " -> "
            pathString += edge.toNode

        print(pathString)

    def printAllNodes(self):
        print(self.name + ":")
        for node in self.nodes:
            print("Node {}: {}, {}".format(node.name,
                                           node.earlyTime,
                                           node.lateTime))

    def printAllEdges(self):
        print(self.name + ":")
        for edge in self.edges:
            print("Edge {}->{}: {}, {}".format(edge.fromNode, edge.toNode, edge.time, round(edge.variance, 2)))
