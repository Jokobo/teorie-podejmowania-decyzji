from graph import Graph
from node import Node
from edge import Edge

fileA = open("data_A.txt", "r")
fileB = open("data_B.txt", "r")

deadline = 48

# Plik A
graphA = Graph("Wariant A")

for line in fileA.read().splitlines():
    tempArray = line.split(' ')
    node = Node(tempArray[0])
    graphA.addNode(node)
    node = Node(tempArray[1])
    graphA.addNode(node)
    edge = Edge(tempArray[0], tempArray[1], float(tempArray[2]), float(tempArray[3]), float(tempArray[4]))
    graphA.addEdge(edge)

# Po obliczeniu średniego czasu oraz wariancji
graphA.printAllNodes()
graphA.printAllEdges()

# Po obliczeniu najkrótszego i najdłuższego czasu realizacji dla każdego węzła
graphA.calculateEarlyTimes()
graphA.calculateLateTimes()
graphA.printAllNodes()

# Po znalezieniu ścieżki
graphA.getCriticalPath()
graphA.printCriticalPath()

# Po obliczeniu całkowitego oczekiwanego czasu oraz sumy wariancji
graphA.calculateStdDevFromCriticalPath()
zScoreA = graphA.calculateZScoreForGivenValue(deadline)

print("Czas: {}, odchylenie standardowe: {}".format(graphA.time, round(graphA.stddev, 4)))
print("Wartość z dla terminu {} dni (Wariant A): {}".format(deadline, round(zScoreA, 3)))
#z = -0.702 --> P = 0.2420 --> 24.20%

# Plik B
graphB = Graph("Wariant B")

for line in fileB.read().splitlines():
    tempArray = line.split(' ')
    node = Node(tempArray[0])
    graphB.addNode(node)
    node = Node(tempArray[1])
    graphB.addNode(node)
    edge = Edge(tempArray[0], tempArray[1], float(tempArray[2]), float(tempArray[3]), float(tempArray[4]))
    graphB.addEdge(edge)

# Po obliczeniu średniego czasu oraz wariancji
graphB.printAllNodes()
graphB.printAllEdges()

# Po obliczeniu najkrótszego i najdłuższego czasu realizacji dla każdego węzła
graphB.calculateEarlyTimes()
graphB.calculateLateTimes()
graphB.printAllNodes()

# Po znalezieniu ścieżki
graphB.getCriticalPath()
graphB.printCriticalPath()

# Po obliczeniu całkowitego oczekiwanego czasu oraz sumy wariancji
graphB.calculateStdDevFromCriticalPath()
zScoreB = graphB.calculateZScoreForGivenValue(deadline)

print("Czas: {}, odchylenie standardowe: {}".format(graphB.time, round(graphB.stddev, 4)))
print("Wartość z dla terminu {} dni (Wariant B): {}".format(deadline, round(zScoreB, 3)))
#z = -2.0 --> P = 0.0228 --> 2.28%
